package org.collective.testmethods;

import java.awt.AWTException;
import java.io.IOException;

import org.collective.admin.pageobjects.CollectiveAdminAnalyticScriptsPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminBlogPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminCarouselPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminHomePageObjects;
import org.collective.admin.pageobjects.CollectiveAdminOrdersPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminProductsPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminPromotionsPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminReportPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminReturnsPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminSettingsPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminStoreLocatorPageObjects;
import org.collective.admin.pageobjects.CollectiveAdminUsersPageObjects;
import org.collective.customer.pageobjects.CollectiveHomePageObjects;
import org.collective.customer.pageobjects.CollectiveLoginPageObjects;

import org.collective.maincontroller.MainController;
import org.collective.utils.TestUtility;
import org.testng.annotations.Test;

public class CollectiveRegressionTestAdmin extends MainController{
	  
	/*
	 * @author Hemanth.Sridhar
	 */
	
	
  @Test(alwaysRun = true)
  public void admin_tc001_collective_adminLogin() throws IOException, InterruptedException, AWTException {
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc002_collective_admin_reports_OrderTotalsSheet() throws InterruptedException, IOException
  {
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminReportPageObjects reportsPage = new CollectiveAdminReportPageObjects(driver);
	  
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnReports();
	  reportsPage.clickOrderTotals();
	  reportsPage.verifyOrderTotalsHeader();
	  reportsPage.clickDownloadButton();
	  
  }
  
  @Test(alwaysRun = true)
  public void admin_tc003_evaluateAdminTabs_Returns() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminReturnsPageObjects returnsPage = new CollectiveAdminReturnsPageObjects(driver);
	  
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnReturns();
	  returnsPage.verifyReturnsPage();
	  
  }	
  
  @Test(alwaysRun = true)
  public void admin_tc004_evaluateAdminTabs_Carousel() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminCarouselPageObjects carouselPage = new CollectiveAdminCarouselPageObjects(driver);
	 
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnCarousel();
	  carouselPage.verifyCarouselPage();
	  
  }	
  
  @Test(alwaysRun = true)
  public void admin_tc005_evaluateAdminTabs_Orders() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminOrdersPageObjects ordersPage = new CollectiveAdminOrdersPageObjects(driver); 
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnOrders();
	  ordersPage.verifyOrdersPage();
  }	
  
  @Test(alwaysRun = true)
  public void admin_tc006_evaluateAdminTabs_Products_ProductsSection() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  productsPage.verifyProductsPageProductsSection();
	  
  }	
  
  @Test(alwaysRun = true)
  public void admin_tc007_evaluateAdminTabs_Settings() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminSettingsPageObjects settingsPage = new CollectiveAdminSettingsPageObjects(driver);
	 
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnSettings();
	  settingsPage.verifySettingsPage();
	 
  }
  
  
  @Test(alwaysRun = true)
  public void admin_tc008_evaluateAdminTabs_Promotions() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminPromotionsPageObjects promotionsPage = new CollectiveAdminPromotionsPageObjects(driver);  
	  
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnPromotions();
	  promotionsPage.verifyPromotionsPage();
	  
  }
  
  @Test(alwaysRun = true)
  public void admin_tc009_evaluateAdminTabs_Users() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver);  
	 
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnUsers();
	  usersPage.verifyUsersPage();
	 
  }
  
  @Test(alwaysRun = true)
  public void tc010_evaluateAdminTabs_Blog() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminBlogPageObjects blogsPage = new CollectiveAdminBlogPageObjects(driver); 
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnBlog();
	  blogsPage.verifyBlogPage();
	 adminHomePage.adminLogout();
	  homePage.verifyLogout();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc011_evaluateAdminTabs_Store() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	 
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnStores();
	  storesPage.verifyStoresPage();
	
  }
  
  @Test(alwaysRun = true)
  public void admin_tc012_Login_ErrorScenario_Providing_InvalidCredentials() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgInvalidPhoneNumberAndPwd();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc013_Login_ErrorScenario_Providing_CustomerCredentials() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgInvalidPhoneNumberAndPwd();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc014_Login_ErrorScenario_Providing_NoPassword() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgBlankPhoneNumberOrPwdOrBothBlank();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc015_Login_ErrorScenario_Providing_CustomerCredentials_NoPhoneNumber() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPasswordForLogin();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgBlankPhoneNumberOrPwdOrBothBlank();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc016_Login_ErrorScenario_Providing_CustomerCredentials_NoPhoneNumber_NoPassword() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.clickLogin();
	  loginPage.verifyErrorMsgBlankPhoneNumberOrPwdOrBothBlank();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc017_StoreLocator_Adding_RemovingNewStore() throws IOException, InterruptedException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnStores();
	  storesPage.verifyStoresPage();
	  storesPage.clickNewStoreButton();
	  storesPage.enterCity();
	  storesPage.enterAddress1();
	  storesPage.enterAddress2();
	  storesPage.enterPhoneNumber();
	  storesPage.clickCreate();
	  storesPage.verifyStoreCreate();
	  storesPage.deleteStore();
	  storesPage.switchToAlertAndAccept();
	  Thread.sleep(3000);
	  storesPage.assertAfterDelete();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc018_StoreLocator_Adding_Updating_RemovingNewStore() throws IOException, InterruptedException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnStores();
	  storesPage.verifyStoresPage();
	  storesPage.clickNewStoreButton();
	  storesPage.enterCity();
	  storesPage.enterAddress1();
	  storesPage.enterAddress2();
	  storesPage.enterPhoneNumber();
	  storesPage.clickCreate();
	  storesPage.verifyStoreCreate();
	  storesPage.clickNewCreatedStore();
	  storesPage.enterAddress3();
	  storesPage.clickUpdate();
	  storesPage.clickBackToStoresList();
	  storesPage.clickNewCreatedStore();
	  storesPage.verifyUpdate();
	  storesPage.clickBackToStoresList();
	  storesPage.deleteStore();
	  storesPage.switchToAlertAndAccept();
	  Thread.sleep(3000);
	  storesPage.assertAfterDelete();
  }
  
  
  @Test(alwaysRun = true)
  public void admin_tc019_NewBlogEntry_RemoveBlog() throws IOException, InterruptedException, AWTException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminBlogPageObjects blogsPage = new CollectiveAdminBlogPageObjects(driver); 
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnBlog();
	  blogsPage.verifyBlogPage();
	  blogsPage.clickNewBlogEntry();
	  blogsPage.enterBlogTitle();
	  blogsPage.enterClickPublishedAt();
	  blogsPage.clickCurrentDate();
	  Thread.sleep(2000);
	  blogsPage.enterBodyAndSummary();
	  blogsPage.uploadImage();
	  blogsPage.clickVisibleCheckbox();
	  blogsPage.clickCreate();
	  blogsPage.verifyBlogCreate();
	  TestUtility.openPageInNewTab();
	  homePage.switchToLatestWindow();
	  blogsPage.openBlogsPage();
	  blogsPage.verifyNewBlogCreated();
	  blogsPage.closePage();
	  homePage.switchToPreviousTab();
	  blogsPage.clickBackToBlogList();
	  blogsPage.clickFirstDelete();
	  storesPage.switchToAlertAndAccept();
	  //blogsPage.verifyBlogDelete();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc020_NewUsersCreation_User_And_Deletion() throws IOException, InterruptedException, AWTException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnUsers();
	  usersPage.verifyUsersPage();
	  usersPage.clickNewUser();
	  usersPage.enterEmailID();
	  usersPage.enterPassword();
	  usersPage.enterConfirmPassword();
	  usersPage.enterUniquePhoneNumber();
	  usersPage.clickUserCheckbox();
	  usersPage.clickCreate();
	  usersPage.verifyCreationOfUser();
	  usersPage.clickBackToUsersList();
	  usersPage.searchForEmailID();
	  usersPage.clickSearchButton();
	  usersPage.clickDeleteButton();
	  storesPage.switchToAlertAndAccept();
	  //usersPage.verifyUserDelete();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc021_NewUsersCreation_Admin_And_Deletion() throws IOException, InterruptedException, AWTException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnUsers();
	  usersPage.verifyUsersPage();
	  usersPage.clickNewUser();
	  usersPage.enterEmailID();
	  usersPage.enterPassword();
	  usersPage.enterConfirmPassword();
	  usersPage.enterUniquePhoneNumber();
	  usersPage.clickAdminCheckbox();
	  usersPage.clickCreate();
	  usersPage.verifyCreationOfUser();
	  usersPage.clickBackToUsersList();
	  usersPage.searchForEmailID();
	  usersPage.clickSearchButton();
	  usersPage.clickDeleteButton();
	  storesPage.switchToAlertAndAccept();
	  usersPage.verifyUserDelete();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc022_NewUsersCreation_Blogger_And_Deletion() throws IOException, InterruptedException, AWTException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnUsers();
	  usersPage.verifyUsersPage();
	  usersPage.clickNewUser();
	  usersPage.enterEmailID();
	  usersPage.enterPassword();
	  usersPage.enterConfirmPassword();
	  usersPage.enterUniquePhoneNumber();
	  usersPage.clickBloggerCheckbox();
	  usersPage.clickCreate();
	  usersPage.verifyCreationOfUser();
	  usersPage.clickBackToUsersList();
	  usersPage.searchForEmailID();
	  usersPage.clickSearchButton();
	  usersPage.clickDeleteButton();
	  storesPage.switchToAlertAndAccept();
	  usersPage.verifyUserDelete();
  }
  
  /*
   * search name,code,path pending for blogger
   */
  
  @Test(alwaysRun = true)
  public void admin_tc023_NewPromotion_Creation_And_Deletion() throws IOException, InterruptedException, AWTException{
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminPromotionsPageObjects promotionsPage = new CollectiveAdminPromotionsPageObjects(driver); 
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnPromotions();
	  promotionsPage.clickNewPromotion();
	  promotionsPage.enterName();
	  promotionsPage.enterCode();
	  promotionsPage.enterDescription();
	  promotionsPage.enterUsageLimit();
	  usersPage.clickCreate();
	  promotionsPage.verifyPromotionCreate();
	  promotionsPage.clickBackToPromotionsList();
	  usersPage.clickDeleteButton();
	  storesPage.switchToAlertAndAccept();
	  promotionsPage.verifyPromotionDelete(); 
  }
  
  
 
  @Test(alwaysRun = true)
  public void admin_tc023_evaluateAdminTabs_AnalyticScript() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminAnalyticScriptsPageObjects analyticScriptPage = new CollectiveAdminAnalyticScriptsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  analyticScriptPage.clickAnalyticScript();
	  analyticScriptPage.verifyAnalyticScriptPage();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc024_CreateNew_AnalyticScript() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminAnalyticScriptsPageObjects analyticScriptPage = new CollectiveAdminAnalyticScriptsPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  analyticScriptPage.clickAnalyticScript();
	  analyticScriptPage.verifyAnalyticScriptPage();
	  analyticScriptPage.clickAddAnalyticScript();
	  analyticScriptPage.enterAnalyticScriptName();
	  analyticScriptPage.enterAnalyticScriptCode();
	  analyticScriptPage.clickCreate();
	  analyticScriptPage.clickDelete();
	  storesPage.switchToAlertAndAccept();
  }
  
  @Test(alwaysRun = true)
  public void admin_tc025_CreateNew_AnalyticScript() throws IOException{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminAnalyticScriptsPageObjects analyticScriptPage = new CollectiveAdminAnalyticScriptsPageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  analyticScriptPage.clickAnalyticScript();
	  analyticScriptPage.verifyAnalyticScriptPage();
	  analyticScriptPage.clickAddAnalyticScript();
	  analyticScriptPage.enterAnalyticScriptName();
	  analyticScriptPage.enterAnalyticScriptCode();
	  analyticScriptPage.clickCreate();
	  analyticScriptPage.clickDelete();
	  storesPage.switchToAlertAndAccept();
  }
  
  @Test
  public void admin_tc026_UserList() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminReportPageObjects reportsPage = new CollectiveAdminReportPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnReports();
	  adminHomePage.clickUsersList();
	  reportsPage.verifyUsersListHeader();
	  reportsPage.clickDownloadButton();
  }
  
  @Test
  public void admin_tc027_returnList() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminReportPageObjects reportsPage = new CollectiveAdminReportPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnReports();
	  adminHomePage.clickReturnList();
	  reportsPage.verifyReturnsListHeader();
	  reportsPage.clickDownloadButton();
  }
  
  @Test
  public void admin_tc028_Products_Taxonomies() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  adminHomePage.navigateTo_taxonomies();
	  productsPage.verifyTaxonomiesPage();
  }
  
  @Test
  public void admin_tc029_Products_Taxons() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  adminHomePage.navigateTo_taxons();
	  productsPage.verifyTaxons();
  }
 
  @Test
  public void admin_tc030_CreateVerifyAndDeleteCarousel() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminCarouselPageObjects carouselPage = new CollectiveAdminCarouselPageObjects(driver);
	  CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
	  CollectiveAdminStoreLocatorPageObjects storesPage = new CollectiveAdminStoreLocatorPageObjects(driver);
	  CollectiveAdminBlogPageObjects blogsPage = new CollectiveAdminBlogPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnCarousel();
	  carouselPage.clickAddSlide();
	  carouselPage.enterCarouselName();
	  carouselPage.uploadCover();
	  carouselPage.uploadSmallImage();
	  carouselPage.enterDescription();
	  carouselPage.enterCarouselLink();
	  carouselPage.clickCreate();
	  carouselPage.waitForCreationOfCarousel();	  
	  TestUtility.openPageInNewTab();
	  homePage.switchToLatestWindow();
	  homePage.openApplication();
	  homePage.verifyCarousel();
	  blogsPage.closePage();
	  homePage.switchToPreviousTab();
	  homePage.deleteCarousel();
	  storesPage.switchToAlertAndAccept();
	  
  }
  
  @Test
  public void admin_tc031_CancelCarousel() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminCarouselPageObjects carouselPage = new CollectiveAdminCarouselPageObjects(driver);
	  CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnCarousel();
	  carouselPage.clickAddSlide();
	  usersPage.clickCancel();
	  carouselPage.waitForCreationOfCarousel();
  }
  
  @Test
  public void admin_tc032_Products_OptionTypes() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  adminHomePage.navigateTo_optionTypes();
	  productsPage.verifyOptionTypes();
  }
  
  @Test
  public void admin_tc033_Products_properties() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  adminHomePage.navigateTo_properties();
	  productsPage.verifyProperties();
  }
  
  
  @Test
  public void admin_tc034_Products_prototypes() throws Exception{
	  CollectiveAdminHomePageObjects adminHomePage = new CollectiveAdminHomePageObjects(driver); 
	  CollectiveLoginPageObjects loginPage = new CollectiveLoginPageObjects(driver);
	  CollectiveAdminProductsPageObjects productsPage = new CollectiveAdminProductsPageObjects(driver);
	  adminHomePage.openAdminPage();
	  loginPage.enterPhoneNumberForAdmin();
	  loginPage.enterPasswordForAdmin();
	  loginPage.clickLogin();
	  loginPage.verifyAdminLoginMsg();
	  adminHomePage.clickOnProducts();
	  adminHomePage.navigateTo_prototypes();
	  productsPage.verifyPrototypes();
  }
}