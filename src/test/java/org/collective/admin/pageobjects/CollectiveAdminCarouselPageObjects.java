package org.collective.admin.pageobjects;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminCarouselPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminCarouselPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@class='row']")
	private WebElement carouselDiv;
	
	@FindBy(xpath="//h1[contains(text(),'Carousel')]")
	private WebElement carouselHeader;
	
	@FindBy(xpath="//a[@id='admin_new_user_link']")
	private WebElement carouselAddSlideButton;
	
	@FindBy(xpath="//input[@id='carousel_name']")
	private WebElement carouselName;
	
	@FindBy(xpath="//input[@id='carousel_description']")
	private WebElement carouselDescription;
	
	@FindBy(xpath="//input[@id='carousel_link']")
	public WebElement carouselLink;
	
	@FindBy(xpath="//input[@id='carousel_cover']")
	private WebElement carouselCover;
	
	@FindBy(xpath="//input[@id='carousel_small_image']")
	private WebElement carouselSmallImage;
	
	@FindBy(xpath="//button[contains(text(),'Create')]")
	private WebElement createButton;
	
	public void verifyCarouselPage() {
		Waiting.explicitWaitVisibilityOfElement(carouselHeader, 6);
		Assert.assertTrue(carouselHeader.isDisplayed());
		Assert.assertTrue(carouselDiv.isDisplayed());
		Assert.assertTrue(carouselAddSlideButton.isDisplayed());
	}

	public void clickAddSlide() {
		Waiting.explicitWaitVisibilityOfElement(carouselAddSlideButton, 5);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",carouselAddSlideButton);
		
	}

	public void enterCarouselName() {
		carouselName.sendKeys(data.getCaraouselName());
	}

	public void uploadCover() throws Exception 
	{
		carouselCover.click();
		StringSelection stringSelection = new StringSelection("D:\\workspace\\CollectiveAutomationSuiteRegression\\resources\\Images\\CarouselCover.jpg");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		 Robot robot = new Robot();
		 Thread.sleep(1500);
		robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        Thread.sleep(2000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
		//TestUtility.fileUpload(data.getCarouselFilePath());
		//carouselCover.sendKeys(data.getCarouselFilePath());
		
	}

	public void uploadSmallImage() throws AWTException, InterruptedException {
		carouselSmallImage.click();
		StringSelection stringSelection = new StringSelection("D:\\workspace\\CollectiveAutomationSuiteRegression\\resources\\Images\\smallimage.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		 Robot robot = new Robot();
		 Thread.sleep(1500);
		robot.keyPress(KeyEvent.VK_CONTROL);
       robot.keyPress(KeyEvent.VK_V);
       robot.keyRelease(KeyEvent.VK_V);
       robot.keyRelease(KeyEvent.VK_CONTROL);
       Thread.sleep(1500);
       robot.keyPress(KeyEvent.VK_ENTER);
       robot.keyRelease(KeyEvent.VK_ENTER);
       Thread.sleep(3000);
       
	}

	public void enterDescription() {
		carouselDescription.sendKeys(data.getCarouselDescription());
		
	}
	
	public void enterCarouselLink(){
		carouselLink.sendKeys(data.getCarouselLink());
	}

	public void waitForCreationOfCarousel() {
		Waiting.explicitWaitVisibilityOfElement(carouselAddSlideButton, 50);
		
	}

	public void clickCreate() {
		createButton.submit();
		
	}

	public void getCurrentWindowHandle() {
		String parentHandle = driver.getWindowHandle();
		driver.switchTo().window(parentHandle);  
		
	}
	
	public void switchBackToParentWindow(){
		getCurrentWindowHandle();
	}
	}

	
