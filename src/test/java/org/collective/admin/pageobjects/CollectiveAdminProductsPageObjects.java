package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminProductsPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminProductsPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@id='admin_new_product']")
	private WebElement newProductButton;
	
	@FindBy(xpath="//h1[contains(text(),'Listing Products')]")
	private WebElement productsHeader;
	
	@FindBy(xpath="//label[text()='Name']")
	private WebElement nameLabel;
	
	@FindBy(xpath="//input[@id='q_name_cont']")
	private WebElement nameTextBox;
	
	@FindBy(xpath="//input[@id='q_variants_including_master_sku_cont']")
	private WebElement skuTextBox;
	
	@FindBy(xpath="//label[text()='SKU']")
	private WebElement SKUlabel;
	
	@FindBy(xpath="//input[@id='q_deleted_at_null']/ancestor::label")
	private WebElement showDeletedLabel;
	
	@FindBy(xpath="//button[text()='Search']")
	private WebElement searchButton;
	
	@FindBy(xpath="//h1[contains(text(),'Taxonomies')]")
	private WebElement taxonomiesHeader;
	
	@FindBy(xpath="//div[@class='row']")
	private WebElement taxonomiesDiv;
	
	@FindBy(xpath="//a[@id='admin_new_taxonomy_link']")
	private WebElement newTaxonomyButton;
	
	@FindBy(xpath="//div[@id='s2id_taxon_id']")
	private WebElement taxonDropdown;
	
	@FindBy(xpath="//legend")
	private WebElement legendTag;
	
	
	@FindBy(xpath="//h1[contains(text(),'Option Types')]")
	private WebElement optionTypesHeader;
	
	@FindBy(xpath="//a[@id='new_option_type_link']")
	private WebElement newOptionTypeButton;
	
	@FindBy(xpath="//div[@class='row']")
	private WebElement optionTypesDiv;
	
	@FindBy(xpath="//h1[contains(text(),'Properties')]")
	private WebElement propertiesHeader;
	
	@FindBy(xpath="//li[@id='new_property_link']")
	private WebElement newPropertyButton;
	
	@FindBy(xpath="//h1[contains(text(),'Prototypes')]")
	private WebElement prototypesHeader;
	
	@FindBy(xpath="//li[@id='new_prototype_link']")
	private WebElement newPrototypeButton;
	
	public void verifyProductsPageProductsSection() {
		Waiting.explicitWaitVisibilityOfElement(productsHeader, 6);
		Assert.assertTrue(productsHeader.isDisplayed());
		Assert.assertTrue(newProductButton.isDisplayed());
		Assert.assertTrue(nameLabel.isDisplayed());
		Assert.assertTrue(nameTextBox.isDisplayed());
		Assert.assertTrue(skuTextBox.isDisplayed());
		Assert.assertTrue(SKUlabel.isDisplayed());
		Assert.assertTrue(showDeletedLabel.isDisplayed());
		Assert.assertTrue(searchButton.isDisplayed());
	}

	public void verifyTaxonomiesPage() {
		Waiting.explicitWaitVisibilityOfElement(taxonomiesHeader, 5);
		Assert.assertTrue(taxonomiesHeader.isDisplayed(), "taxonomies header is not displayed");
		Assert.assertTrue(taxonomiesDiv.isDisplayed(), "taxonomies table is not displayed");
		Assert.assertTrue(newTaxonomyButton.isDisplayed(),"new taxonomy button is not displayed");
		
	}

	public void verifyTaxons() {
		Assert.assertEquals(legendTag.getText().trim(),data.getTaxonLegendText());
		Assert.assertTrue(taxonDropdown.isDisplayed(), "taxon dropdown is not displayed");
	}

	public void verifyOptionTypes() {
		Assert.assertTrue(optionTypesHeader.isDisplayed(),"option type header is not displayed");
		Assert.assertTrue(optionTypesDiv.isDisplayed(),"option type div is not displayed");
		Assert.assertTrue(newOptionTypeButton.isDisplayed(),"option type button is not displayed");
		
	}

	public void verifyProperties() {
		Assert.assertTrue(propertiesHeader.isDisplayed(),"properties header is not displayed");
		Assert.assertTrue(newPropertyButton.isDisplayed(),"new Property Button  is not displayed");
		Assert.assertTrue(taxonomiesDiv.isDisplayed(),"properties div is not displayed");
	}

	public void verifyPrototypes() {
		Assert.assertTrue(taxonomiesDiv.isDisplayed(),"prototypes div is not displayed");
		Assert.assertTrue(prototypesHeader.isDisplayed(),"prototypes header is not displayed");
		Assert.assertTrue(newPrototypeButton.isDisplayed(),"new Prototype Button is not displayed");
		
		
	}
	}

	
