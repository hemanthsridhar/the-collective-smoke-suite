package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.ApplicationSetUp;
import org.collective.utils.SearchData;
import org.collective.utils.TodaysDetails;
import org.collective.utils.Waiting;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminBlogPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminBlogPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//h1[contains(text(),'Blog Entries')]")
	private WebElement blogHeader;
	
	@FindBy(xpath="//a[text()='New Blog Entry']")
	private WebElement newBlogEntryButton;
	
	@FindBy(xpath="//input[@id='blog_entry_title']")
	private WebElement blogTitle;

	@FindBy(xpath="//input[@id='blog_entry_published_at']")
	private WebElement publishAt;
	
	public String getCurrentDate(){
		if(TodaysDetails.currentDate().startsWith("0"))
		{
			return TodaysDetails.currentDate().substring(1);
		}
		else
		{
			return TodaysDetails.currentDate();
		}
	}
	
    String tableDef = "//td/a[text()='"+getCurrentDate()+"']";
    
    @FindBy(xpath="//div[@id='ui-datepicker-div']")
    private WebElement publishAtCalender;
    
    @FindBy(xpath="//iframe[@title='Rich Text Editor, blog_entry_body']")
    private WebElement blogBody;
    
    @FindBy(xpath="//textarea[@id='blog_entry_summary']")
    private WebElement blogSummary;
    
    @FindBy(xpath="//input[@id='blog_entry_blog_entry_image_attributes_attachment']")
    private WebElement uploadBrowse;
    
    @FindBy(xpath="//button[text()='Create']")
    private WebElement createButton;
    
    @FindBy(xpath="//input[@id='blog_entry_visible']")
    private WebElement visibleCheckBox;
    
    @FindBy(xpath="(//div/h3/a)[1]")
    private WebElement firstBogTitle;
    
    @FindBy(xpath="(//div/p)[1]")
    private WebElement firstBlogParagraph;
    
    @FindBy(xpath="(//td/a[@data-action='remove'])[1]")
    private WebElement firstRemoveButton;
    
    @FindBy(xpath="//span[contains(text(),'Back To Blog List')]")
    private WebElement backToBlogListButton;
    
    @FindBy(xpath="//div[contains(text(),'Blog Entry has been successfully removed!')]")
    private WebElement blogEntryDeleteAlert;
    
    @FindBy(xpath="//div[contains(text(),'Blog Entry has been successfully created!')]")
    private WebElement blogEntryCreate;
    
    ApplicationSetUp setup = new ApplicationSetUp();
    
	public void verifyBlogPage() {
		Waiting.explicitWaitVisibilityOfElement(blogHeader, 6);
		Assert.assertTrue(blogHeader.isDisplayed());
		Assert.assertTrue(newBlogEntryButton.isDisplayed());
	}


	public void clickNewBlogEntry() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",newBlogEntryButton);
		
	}


	public void enterBlogTitle() {
		blogTitle.clear();
		blogTitle.sendKeys(data.getblogTitle());
		
	}


	public void enterClickPublishedAt() {
		publishAt.click();
		
	}


	public void clickCurrentDate() {
	Waiting.explicitWaitVisibilityOfElement(publishAtCalender, 10);
	driver.findElement(By.xpath(tableDef)).click();
		
	}


	public void enterBodyAndSummary() {
		driver.switchTo().frame(blogBody);
		WebElement body = driver.switchTo().activeElement();
		body.sendKeys(data.getblogBody());
		enterSummary(body);
		driver.switchTo().defaultContent();
		
	}


	private void enterSummary(WebElement body) {
		body.sendKeys(Keys.TAB,data.getBlogSummary());
		
	}


	public void uploadImage() {
		
		uploadBrowse.sendKeys(data.getBlogImageFilePath());
	}


	public void clickCreate() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",createButton);
		
	}


	public void openBlogsPage() {
		driver.get(setup.getURL()+"/"+"blogs");
	}


	public void clickVisibleCheckbox() {
		visibleCheckBox.click();
		
	}


	public void verifyNewBlogCreated() {
		Assert.assertEquals(firstBogTitle.getText().trim(),data.getblogTitle());
		Assert.assertEquals(firstBlogParagraph.getText().trim(), data.getBlogSummary());
		
	}


	public void closePage() {
		driver.close();
	}


	public void clickFirstDelete() {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",firstRemoveButton);
	}


	public void clickBackToBlogList() {
		Waiting.explicitWaitVisibilityOfElement(backToBlogListButton, 5);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",backToBlogListButton);
		
	}


	public void verifyBlogDelete() {
		Waiting.explicitWaitVisibilityOfElement(blogEntryDeleteAlert, 5);
		Assert.assertEquals(blogEntryDeleteAlert.getText().trim(), data.getBlogEntryDeleteAlert());
		
	}


	public void verifyBlogCreate() {
		Assert.assertEquals(blogEntryCreate.getText().trim(), data.getBlogEntryCreate());
		
	}
	}

	
