package org.collective.admin.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
/*
 * @author Hemanth.Sridhar
 */
public class CollectiveAdminPromotionsPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveAdminPromotionsPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//h1[contains(text(),'Promotions')]")
	private WebElement promotionsHeader;
	
	@FindBy(xpath="//a[text()='New Promotion']")
	private WebElement newPromotionButton;
	
	@FindBy(xpath="//label[text()='name']")
	private WebElement nameLabel;
	
	@FindBy(xpath="//input[@id='q_name_cont']")
	private WebElement nameTextBox;
	
	@FindBy(xpath="//label[text()='code']")
	private WebElement codeLabel;
	
	@FindBy(xpath="//input[@id='q_code_cont']")
	private WebElement codeTextbox;
	
	@FindBy(xpath="//label[text()='path']")
	private WebElement pathLabel;

	@FindBy(xpath="//input[@id='q_path_cont']")
	private WebElement pathTextbox;
	
	@FindBy(xpath="//button[text()='Filter Results']")
	private WebElement filterResultsButton;

	@FindBy(xpath="//input[@id='promotion_name']")
	private WebElement name;
	
	@FindBy(xpath="//input[@id='promotion_code']")
	private WebElement code;
	
	@FindBy(xpath="//input[@id='promotion_usage_limit']")
	private WebElement usageLimit;
	
	@FindBy(xpath="//a[contains(text(),'Back To Promotions List')]")
	private WebElement backToPromotionsList;
	
	
	public void verifyPromotionsPage() {
		Waiting.explicitWaitVisibilityOfElement(promotionsHeader, 6);
		Assert.assertTrue(promotionsHeader.isDisplayed());
		Assert.assertTrue(newPromotionButton.isDisplayed());
		Assert.assertTrue(nameLabel.isDisplayed());
		Assert.assertTrue(nameTextBox.isDisplayed());
		Assert.assertTrue(codeLabel.isDisplayed());
		Assert.assertTrue(codeTextbox.isDisplayed());
		Assert.assertTrue(pathLabel.isDisplayed());
		Assert.assertTrue(pathTextbox.isDisplayed());
		Assert.assertTrue(filterResultsButton.isDisplayed());
	}


	public void enterName() {
		Waiting.explicitWaitVisibilityOfElement(name, 5);
		name.sendKeys(data.getPromotionsName());
		
	}


	public void enterCode() {
		code.sendKeys(data.getPromotionsCode());
		
	}


	public void enterDescription() {
		code.sendKeys(Keys.TAB,data.getPromotionsDescription());
		
	}


	public void enterUsageLimit() {
		
		usageLimit.sendKeys(data.getUsageLimit());
	}


	public void clickBackToPromotionsList() {
		Waiting.explicitWaitVisibilityOfElement(backToPromotionsList, 5);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",backToPromotionsList);
	}


	public void verifyPromotionCreate() {
		CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
		Assert.assertEquals(usersPage.alertSuccessMsg.getText().trim(),"Promotion "+'"'+data.getPromotionsName()+'"'+" has been successfully created!");
		
	}


	public void verifyPromotionDelete() {
		CollectiveAdminUsersPageObjects usersPage = new CollectiveAdminUsersPageObjects(driver); 
		Waiting.explicitWaitVisibilityOfElement(usersPage.alertSuccessMsg, 5);
		Assert.assertEquals(usersPage.alertSuccessMsg.getText().trim(),"Promotion "+'"'+data.getPromotionsName()+'"'+" has been successfully removed!");
		
	}


	public void clickNewPromotion() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",newPromotionButton);
		
	}
	}

	
