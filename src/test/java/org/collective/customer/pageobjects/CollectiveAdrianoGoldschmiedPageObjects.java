package org.collective.customer.pageobjects;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.collective.maincontroller.MainController;
import org.collective.utils.Waiting;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CollectiveAdrianoGoldschmiedPageObjects extends MainController {

	public CollectiveAdrianoGoldschmiedPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@id='products_page']/ul/li[1]")
	private WebElement firstProduct;
	
	@FindBy(xpath="//button[@id='add-to-cart-button']")
	private WebElement addToCartButton;
	
	@FindBy(xpath="//div[@class='noUi-handle noUi-handle-lower']")
	private WebElement leftSlider;
	
	@FindBy(xpath="(//ul/li[@class='prod-price-discount']/a)[1]")
	private WebElement firstProductDiscountPrice;
	
	@FindBy(xpath="(//ul/li[@class='prod-price']/a)[2]")
	private WebElement secondProductPrice;
	
	@FindBy(xpath="//span[@id='skip-value-lower']")
	private WebElement minVal;
	
	@FindBy(xpath="//input[@id='quantity']")
    private WebElement quantityBox;	
	
	@FindBy(xpath="//div[@id='products_page']")
	private WebElement productsdiv;
	
	@FindAll(value={@FindBy(xpath="//div[@id='products_page']/ul/li")})
	private WebElement productsList;
	
	@FindBy(xpath="//span[text()='Selected Quantity Not Available']")
	private WebElement flashMsg;
	
	@FindBy(xpath="//span[@class='font-med long_description']")
	private WebElement block;
	
	@FindBy(xpath="//button[@id='out-of-stock-button']")
    private WebElement outOfStockBotton;
	
	private String InvalidHtmlMsg="input#quantity:invalid";
	
	public void dragLeftSlider(int x){
		Actions moveSlider = new Actions(driver);
		Action action = (Action) moveSlider.dragAndDropBy(leftSlider, x, 0).build();
		action.perform();
		
	}
	public void clickOnFirstProduct() {
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 6);
		firstProduct.click();
	}
	
	public void clickOnAddToCart(){
		Waiting.explicitWaitVisibilityOfElement(addToCartButton, 5);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",addToCartButton);
	}
	public void verifyPriceFilter() throws ParseException {
		Waiting.explicitWaitVisibilityOfElement(firstProductDiscountPrice, 5);
		
		Number fromProduct = NumberFormat.getNumberInstance(Locale.getDefault()).parse(firstProductDiscountPrice.getText());
		
		Number fromMinWeb = NumberFormat.getNumberInstance(Locale.getDefault()).parse(minVal.getText());
		
		Assert.assertTrue(checkForPrice(fromProduct,fromMinWeb),"price filter is not working");
	}
	
	
	private boolean checkForPrice(Number fromProduct, Number fromMinWeb ) {
		if(fromProduct.intValue() >=fromMinWeb.intValue())
		{	
			return true;
		}
		return false;
	}
	
	public void revertBackToTheInitalStateUsingFilter() {
		Actions moveSlider = new Actions(driver);
		Action action = (Action) moveSlider.dragAndDropBy(leftSlider, -50, 0).build();
		action.perform();
		
	}
	
	public void addNumberForQuantity() {
		Waiting.explicitWaitVisibilityOfElement(quantityBox, 10);
		quantityBox.clear();
		quantityBox.sendKeys(data.getNumberOfProductsForQuantityTC028());
		
	}
	
	public void addDifferentProductsSameCategoryWithSingleQuantity() {
		
		CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
		CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
		CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 
		
		for(int i=1;i<=data.getNumberOfTimesToAddDifferentProduct();i++)
		{
			Waiting.explicitWaitVisibilityOfElement(productsdiv, 6);
			driver.findElement(By.xpath("//div[@id='products_page']/ul/li["+i+"]")).click();
			andrianoPage.clickOnAddToCart();
			homePage.hoverAccessories();
			homePage.hoverAccessoriesMen();
			homePage.navigateToJeansFromMen();
			
		}
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 5);
		shoppingcartPage.cartLinkClick();
		
	}
	public void addDifferentProductsDifferentCategoryWithSingleQuantity() {
		CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
		CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
		CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
		for(int i=1;i<=data.getNumberOfTimesToAddDifferentProduct();i++)
		{
			Waiting.explicitWaitVisibilityOfElement(productsdiv, 6);
			driver.findElement(By.xpath("//div[@id='products_page']/ul/li["+i+"]")).click(); 
			andrianoPage.clickOnAddToCart();
			shoppingcartPage.continueShoppingClick();
			homePage.navigateToPoloFromMen();
		}
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 5);
		shoppingcartPage.cartLinkClick();
		
	}
	
	public void addDifferentProductsDifferentCategoryWithSingleQuantityMoreThan20000() {
		CollectiveShoppingCartPageObjects shoppingcartPage = new CollectiveShoppingCartPageObjects(driver);
		CollectiveAdrianoGoldschmiedPageObjects andrianoPage = new CollectiveAdrianoGoldschmiedPageObjects(driver);
		CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver);
		for(int i=1;i<=data.getNumberOfTimesToAddDifferentProduct();i++)
		{
			Waiting.explicitWaitVisibilityOfElement(productsdiv, 6);
			driver.findElement(By.xpath("//div[@id='products_page']/ul/li["+i+"]")).click(); 
			andrianoPage.clickOnAddToCart();
			shoppingcartPage.continueShoppingClick();
			homePage.navigateToArmaniFromMen();
		    andrianoPage.dragLeftSlider(90);
		}
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 5);
		shoppingcartPage.cartLinkClick();
		
	}
	
	public void addAHugeQuantity() {
	
		quantityBox.clear();
		quantityBox.sendKeys(data.getHugeNumberForQuantityTC054());
	}
	public void verifyErrorMsgForHugeNumberOfQuantity() {
		Waiting.explicitWaitVisibilityOfElement(flashMsg, 3);
		Assert.assertEquals(flashMsg.getText(), data.getVerifyErrorMsgForHugeQuantity());
		
	//Assert.assertTrue(driver.findElement(By.cssSelector(InvalidHtmlMsg)).isDisplayed(),"Error Msg is not displayed");
		
	}
	public void addANegativeNumber() {
		quantityBox.clear();
		((JavascriptExecutor) driver).executeScript("document.getElementById('quantity').value = "+data.getNegativeNumberForQuantityTC055()+";");
		
	}
	public void verifyErrorMsgForNegativeNumberOfQuantity() {
		Assert.assertTrue(driver.findElement(By.cssSelector(InvalidHtmlMsg)).isDisplayed(),"Error Msg is not displayed");
		
	}
	public void clickOutside() {
		
		block.click();
	}
	public void addDecimalValue() {
		quantityBox.clear();
		((JavascriptExecutor) driver).executeScript("document.getElementById('quantity').value = "+data.getDecimalNumberForQuantityTC056()+";");
		
	}
	public boolean verifySoldOutProductAndClickNextProduct() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		try
		{
		if(outOfStockBotton.isDisplayed())
		{
			
			driver.navigate().back();
			Waiting.explicitWaitVisibilityOfElement(secondProductPrice, 5);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",secondProductPrice);
		    return true;
			
		}	
		else
		{
			return true;
		}
		}
		catch(Exception e)
		{
			return true;
		}
	}
}
