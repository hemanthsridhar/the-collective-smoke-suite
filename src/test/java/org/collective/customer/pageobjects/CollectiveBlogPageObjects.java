package org.collective.customer.pageobjects;
import java.util.List;

import org.collective.maincontroller.MainController;
import org.collective.utils.SearchData;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveBlogPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveBlogPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@id='blog_entry_container']")
	private WebElement blogEntryContainer;
	
	@FindAll(value = { @FindBy (xpath="//div[@id='blog_entry_container']/div")})
	private List<WebElement> blogEntryContainerChildren;
	
	@FindAll(value={@FindBy(xpath="//p")})
	private List<WebElement> blogParagraphs;
	
	@FindBy(xpath="(//h3/a)[1]")
	private WebElement firstBlog;
	
	@FindBy(xpath="//div[@class='row']/a[1]")
	private WebElement previousBlog;
	
	@FindBy(xpath="//h2")
	private WebElement blogHeading;
	
	@FindBy(xpath="//div[@class='row']/a[3]")
	private WebElement nextBlog;
	
	@FindBy(xpath="//a[2]")
	private WebElement theBlogButton;
	
	public void verifyBlogPage() {
		Assert.assertTrue(blogEntryContainer.isDisplayed());
		for(int i=0;i<blogEntryContainerChildren.size();i++)
		{
		Assert.assertTrue(blogEntryContainerChildren.get(i).isDisplayed());
		}
		for(int i=0;i<blogParagraphs.size();i++)
		{
		Assert.assertTrue(blogParagraphs.get(i).isDisplayed());
		}
	}
	public void clickOnFirstBlogAndAssertHeading() {
		String  s = firstBlog.getText().trim();
((JavascriptExecutor) driver).executeScript("arguments[0].click();",firstBlog);
  assertPageHeadingForBlogClick(s);
	}

	public void assertNextNavigationClass(){
		Assert.assertEquals(nextBlog.getAttribute("class").replace("\n", "").replace(" ","").trim(), data.getBlogNextClass().trim());
		//System.out.println(s);
		//Assert.assertFalse(nextBlog.isEnabled(), "next Blog is not disabled");
		
	}
	public void clickPreviousBlogNavigation() {
Assert.assertTrue(previousBlog.isDisplayed(), "previous link is not displayed");;
		previousBlog.click();
	     
		
	}

	public void assertNextNavigationClassAfterClickingPrevious() {
Assert.assertEquals(nextBlog.getAttribute("class").replace("\n", "").replace(" ","").trim(), data.getBlogNextClassAfterPrevious().trim());
Actions action = new Actions(driver);
action.moveToElement(theBlogButton);
Assert.assertEquals(theBlogButton.getText().trim(),"THE BLOG");
		
	}

	public void assertPageHeadingForBlogClick(String s) {
		Assert.assertTrue(s.equalsIgnoreCase(blogHeading.getText().trim()), "blog heading doesn't match the blog clicked");
		
	}

	public void clickOnTheBlog() {
		theBlogButton.click();
		
	}		
	}
