package org.collective.customer.pageobjects;
import java.util.ArrayList;
import java.util.List;
import org.collective.maincontroller.MainController;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CollectiveMadeToMeasurePageObjects extends MainController {

	public CollectiveMadeToMeasurePageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
@FindBy(xpath="//div[@class='default']/div")	
private WebElement firstPageMadeToMeasureBlock;

@FindBy(xpath="//a[text()='MADE TO MEASURE']")
private WebElement madeToMeasureLink;

@FindBy(xpath="//div[@class='iscon2']")
private WebElement blockText;

@FindBy(xpath="//a[text()='THE COLLECTIVE ATELIER']")
private WebElement collectiveAtelierLink;

@FindBy(xpath="//a[text()='ARMANI']")
private WebElement armaniLink;

@FindBy(xpath="//a[text()='SANTONI']")
private WebElement santoniLink;

@FindBy(xpath="//a[text()='ETON']")
private WebElement etonLink;

@FindBy(xpath="//a[text()='HACKETT']")
private WebElement hackettLink;

@FindBy(xpath="//button[text()='MAKE AN APPOINTMENT']")
private WebElement makeAnAppointmentButton;

@FindBy(xpath="//button[text()='WATCH VIDEO']")
private WebElement watchVideoButton;

@FindBy(xpath="//a[text()='APPOINTMENTS']")
private WebElement apointmentsLink;

@FindBy(xpath="//div[@class='col-md-5 col-sm-12 col-xs-12 contact']")
private WebElement contactText;

@FindBy(xpath="//h2")
private WebElement makeAnAppointmentHeading;

@FindBy(xpath="//input[@id='Name']")
private WebElement makeAnAppointmentName;

@FindBy(xpath="//input[@id='PhoneNumber']")
private WebElement makeAnAppointmentMobileNumber;

@FindBy(xpath="//input[@id='Email']")
private WebElement makeAnAppointmentEmail;

@FindBy(xpath="//input[@id='submit']")
private WebElement submitButton;

@FindBy(xpath="//p[text()='Thank you for submiting the form. We shall get back to you soon.']")
private WebElement appointmentConfirmationText;

@FindBy(xpath="//div[@class='error_msg']")
private WebElement errorMsg;

@FindBy(xpath="//div[@class='container col-md-7 col-sm-12 col-xs-12 schedule-table']")
private WebElement scheduleTable;

@FindBy(xpath="//div[@class='error_msg']")
private List<WebElement> errorMsg1;

CollectiveHomePageObjects homePage = new CollectiveHomePageObjects(driver); 

public boolean assertMadeToMeasureFirstPage(){
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	
	boolean t=	firstPageMadeToMeasureBlock.getText().replace("\n", "").trim().equals(data.getMadeToMeasureFirstPageText());
	madeToMeasurePage.closePage();
	 homePage.switchToPreviousTab();
	return t;
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		return false;
	}
}


public void clickMadeToMeasure() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",madeToMeasureLink);
	
}

public boolean assertMadeToMeasurePage() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	
	boolean t=	blockText.getText().replace("\n", "").trim().equals(data.getMadeToMeasureAreMadeEqualText());
	madeToMeasurePage.closePage();
	 homePage.switchToPreviousTab();
	return t;
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public void clickCollectiveAtelier() throws InterruptedException {
	Thread.sleep(5000);
	((JavascriptExecutor) driver).executeScript("arguments[0].click()",collectiveAtelierLink);
}

public boolean assertCollectiveAtelierPageText(){
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	
	System.out.println(blockText.getText().replace("\n", "").trim());
	boolean t=blockText.getText().replace("\n", "").trim().equals(data.getTheCollectiveAtelierText());
	madeToMeasurePage.closePage();
	 homePage.switchToPreviousTab();
	 return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;	
	}
}

public boolean assertArmaniLink()
{
	
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = armaniLink.isDisplayed();
	return t;//"Armani Link is not displayed");
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;	
	}
}

public boolean assertSantoniLink()
{
	
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = santoniLink.isDisplayed();
	return t;//"Armani Link is not displayed");
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
public boolean assertEtonLink()
{
	
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = etonLink.isDisplayed();
	return t;//"Armani Link is not displayed");
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public boolean assertHackettLink()
{
	try {
		Thread.sleep(5000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Waiting.explicitWaitVisibilityOfElement(hackettLink, 30);	
	boolean t = hackettLink.isDisplayed();
	return t;//"Armani Link is not displayed");
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}


public void clickArmani() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",armaniLink);
	
	
}

public boolean assertArmaniPage() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = blockText.getText().replace("\n", "").trim().equals(data.getArmaniPageText());
	madeToMeasurePage.closePage();
	 homePage.switchToPreviousTab();
	return t;
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}

}


public void clickSantoni() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",santoniLink);
	
}

public void assertSantoniPage() {
	Assert.assertTrue(makeAnAppointmentButton.isDisplayed(), "Make an appointment button is not displayed");
	Assert.assertTrue(watchVideoButton.isDisplayed(), "Watch Video button is not displayed");
}

public void clickEton() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",etonLink);
	
}

public boolean assertEtonPageMakeAnAppointmentButton() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = makeAnAppointmentButton.isDisplayed();
	return t;
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
public void clickHackett() throws InterruptedException {
	Thread.sleep(3000);
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",hackettLink);
	
}

public boolean assertHackettPage() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Boolean t = blockText.getText().replace("\n", "").trim().equals(data.getHackettPageText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
		  }
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public void clickAppointments() throws InterruptedException {
	Thread.sleep(3000);
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",apointmentsLink);
	
}

public void assertAppointmentPage() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Assert.assertTrue(makeAnAppointmentName.isDisplayed(), "Make an appointment Name text box is not displayed");
	Assert.assertTrue(makeAnAppointmentMobileNumber.isDisplayed(), "Make an appointment Mobile Number text box is not displayed");
	Assert.assertTrue(makeAnAppointmentEmail.isDisplayed(), "Make an appointment Email text box is not displayed");
	Assert.assertTrue(submitButton.isDisplayed(), "Submit button is not displayed");
	Assert.assertEquals(contactText.getText().replace("\n", "").trim(), data.getAppointmentsContactText());
	madeToMeasurePage.closePage();
	homePage.switchToPreviousTab();
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
	}
}
public boolean assertAppointmentPageHeading(){
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Boolean t = makeAnAppointmentHeading.getText().replace("\n", "").trim().equals(data.getAppointmentsHeadingText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
		 }
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public boolean assertScheduleTable(){
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = scheduleTable.isDisplayed();
	return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}


public boolean assertAppointmentPageTabletView()
{
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
		
	Boolean t =contactText.getText().replace("\n", "").trim().equals(data.getAppointmentsContactText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public void enterName() {
	Waiting.explicitWaitVisibilityOfElement(makeAnAppointmentName, 5);
	makeAnAppointmentName.sendKeys(data.getsignupUsername());	
}

public void enterPhoneNumber() {
	makeAnAppointmentMobileNumber.click();
	makeAnAppointmentMobileNumber.sendKeys(data.getsignInPhoneNumber());
	
}

public void enterEmail() {
	makeAnAppointmentMobileNumber.click();
	makeAnAppointmentEmail.sendKeys(data.getsignupEmail());
	
}

public void clickSubmit() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click()",submitButton);
	
}

public boolean assertAppointmentFunctionality() throws InterruptedException {
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	Thread.sleep(10000);
	
	try
	{
		Boolean t = appointmentConfirmationText.getText().replace("\n", "").trim().equals(data.getAppointmentConfirmationText());
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		return t;
	}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
public boolean assertCollectiveAtelierPageMobileView() {
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
try {
	Thread.sleep(3000);
} catch (InterruptedException e1) {

	e1.printStackTrace();
}
	try
	{
	Boolean t = blockText.getText().replace("\n", "").trim().equals(data.getTheCollectiveAtelierText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
	
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
public boolean assertArmaniPageTabletView() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Boolean t = blockText.getText().replace("\n", "").trim().equals(data.getArmaniPageText());
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		homePage.switchToPreviousTab();
		return false;
	}
}

public void ClickMakeAnAppointButton() throws InterruptedException {
	Thread.sleep(3000);
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",makeAnAppointmentButton);
	
}

public void ClickWatchVideoButton() {
	((JavascriptExecutor) driver).executeScript("arguments[0].click();",watchVideoButton);	
}

public void switchToYoutube() {
	ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	driver.switchTo().window(tabs.get(2));
	
}

public void enterInvalidEmail() {
	
	makeAnAppointmentEmail.sendKeys(data.getsignupUsername());
}

public boolean assertAppointmentErrorMsg(String expected) {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	
	boolean t = errorMsg.getText().trim().equals(expected);
	madeToMeasurePage.closePage();
	homePage.switchToPreviousTab();
	return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public void enterInvalidPhoneNumber() {
	makeAnAppointmentMobileNumber.sendKeys(data.getSignUpPhoneNumberMoreThan10());
	
}

public boolean assertAppointmentAllErrorMsgs() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
	
		e1.printStackTrace();
	}
	CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	boolean t = false;
	String s[]= data.getErrorMsgsForAppointmentsAllBlank().split(":");
	for(int i=0;i<errorMsg1.size();i++)
	{
		t = errorMsg1.get(i).getText().trim().equals(s[i]);
	}
	
	madeToMeasurePage.closePage();
	homePage.switchToPreviousTab();
	return t;
}

	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
	
public void closePage() {
	driver.close();
	
}


public boolean assertSantoniPageTabletView() {
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Boolean t = blockText.getText().replace("\n", "").trim().equals(data.getSantoniPageText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
}
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}

public boolean assertEtonPageText() throws InterruptedException {
	Thread.sleep(3000);
CollectiveMadeToMeasurePageObjects madeToMeasurePage = new CollectiveMadeToMeasurePageObjects(driver);
	try
	{
	Boolean t = blockText.getText().replace("\n", "").trim().equals(data.getEtonPageText());
		
		madeToMeasurePage.closePage();
		  homePage.switchToPreviousTab();
		  
		  return t;
		  }
	catch(Exception e)
	{
		madeToMeasurePage.closePage();
		 homePage.switchToPreviousTab();
		 return false;
	}
}
}
