package org.collective.customer.pageobjects;
import java.util.List;

import org.collective.maincontroller.MainController;
import org.collective.utils.VerifyPage;
import org.collective.utils.Waiting;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveCasualJacketPageObjects extends MainController{
	
	public CollectiveCasualJacketPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//div[@id='products_page']")
	private WebElement productsdiv;
	
	@FindBy(xpath="(//ul/a/li)[1]")
	private WebElement differentProduct;
	
	@FindBy(xpath="//li[contains(text(),'ADRIANO GOLDSCHMIED')]")
	private WebElement adrianoGoldschmiedProduct;
	
	@FindAll(value={@FindBy(xpath="//div[@id='products_page']/ul/li")})
	private WebElement productsList;
	
	@FindBy(xpath="//input[@id='ADRIANO_GOLDSCHMIED']")
	private WebElement adrianoGlodschmiedCheckbox;
	
	@FindAll(value={@FindBy(xpath="//a[text()='Armani Jeans Laptop Bag']")})
	private WebElement armaniDisplayCheck;
	
	@FindBy(xpath="//ul[@class='image-list no-pad clearfix']/li[1]")
	private WebElement firstProduct;
	
	@FindBy(css="a[href='/t/categories/men/bags?page=2']")
	private WebElement paginatorNextClick;
	
	@FindAll(value = @FindBy(xpath="//li/a[2]"))
	private List<WebElement> categoryType;
	
	@FindBy(xpath="//div[@id='brands']")
	private WebElement brands;
	
	@FindBy(xpath="//div[@id='size_color']/div[1]")
	private WebElement size;
	
	@FindBy(xpath="//div[@id='size_color']/div[2]")
	private WebElement colour;
	
	@FindBy(xpath="//span[text()='Color']/ancestor::h6/following-sibling::ul/li/a[@class='option-value in-stock selected']")
	private WebElement colourChosen;

	@FindBy(xpath="//span[text()='Size']/ancestor::h6/following-sibling::ul/li/a[@class='option-value in-stock selected']")
	private WebElement sizeChosen;

	public void assertForDifferentProduct(){
		Waiting.explicitWaitVisibilityOfElement(differentProduct, 10);
		Assert.assertTrue(differentProduct.isDisplayed(), "different product is not displayed");
		Assert.assertNotEquals(differentProduct.getText().trim(),data.getFilterBrandAssert());
	}
	
	public void adrianoGoldschmiedCheckboxClick(){
		adrianoGlodschmiedCheckbox.click();
	}
	
	public void filterTest(){
		
		Waiting.explicitWaitVisibilityOfElement(adrianoGoldschmiedProduct, 10);
		Assert.assertTrue(adrianoGoldschmiedProduct.isDisplayed(), "adrianoGoldschmied product is not displayed");
		Assert.assertEquals(adrianoGoldschmiedProduct.getText().trim(),"ADRIANO GOLDSCHMIED");
	  
	}
	
	public void clickFirstProduct(){
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 10);
		firstProduct.click();
	}
	
	public void chooseSize() {
		Waiting.explicitWaitVisibilityOfElement(size, 5);
		driver.findElement(By.xpath("//input[@id='"+data.getSize()+"']")).click();
	}
	
	public void verifySize() {
Assert.assertEquals(sizeChosen.getText().trim(), data.getSize(),data.getSize()+" "+"is not the selected one, instead"+" "+sizeChosen.getText().trim()+" "+"is chosen.");
	}	
	
	public void chooseColour() {
		Waiting.explicitWaitVisibilityOfElement(colour, 5);
		WebElement chooseColour=driver.findElement(By.xpath("//input[@id='"+data.getColour()+"']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",chooseColour);
		
	}
	
	public void verifyColour()
	{
		Assert.assertEquals(colourChosen.getText().trim(), data.getColour(),data.getColour()+" "+"is not the selected one, instead"+" "+colourChosen.getText().trim()+" "+"is chosen.");
	}

	public void verifyCasualJacketPage() {
		VerifyPage verify = new VerifyPage(driver);
		verify.verifyFivePktDenimJeansPage("CASUAL JACKET");
		
	}
		
	}
		

	
