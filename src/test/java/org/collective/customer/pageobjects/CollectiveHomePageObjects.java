package org.collective.customer.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.ApplicationSetUp;
import org.collective.utils.RandomNumberGenerator;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveHomePageObjects extends MainController{
	
   SearchData data = new SearchData();
   Actions action = new Actions(driver);
   ApplicationSetUp application = new ApplicationSetUp();
   
	public CollectiveHomePageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[contains(text(),'Accessories')]")
	private WebElement accessories;
	
	@FindBy(xpath="//a[contains(text(),'Accessories')]/following-sibling::ul/descendant::a[contains(text(),'Men')]")
	private WebElement accessoriesMen;
	
	@FindBy(xpath="//a[contains(text(),'Sale')]")
	private WebElement sales;
	
	@FindBy(xpath="//a[@href='/account']")
    public WebElement myAccountXpath;
	
	@FindBy(xpath="//div[contains(text(),'Signed out successfully.')]")
	private WebElement logoutAlert;
	
	@FindBy(xpath="//a[contains(text(),'Men')]")
	private WebElement menTab;
	
	@FindBy(xpath="//a[contains(text(),'Women')]")
	private WebElement womenTab;
	
	@FindBy(xpath="//div[@class='row block']")
	private WebElement blogDiv;
	
	@FindBy(xpath="//h2/b")
	private WebElement blogStoriesHeader;
	
	@FindBy(xpath="//div[@class='row no-margin-in-small-screen static-container']/div")
	private WebElement div;
	
	@FindBy(css="a[href='/static_pages/faqs']")
	private WebElement FAQLink;
	
	
	@FindBy(css="a[href='/static_pages/shippingpolicy']")
	private WebElement shippingPolicyLink;
	
	
	@FindBy(css="a[href='/static_pages/returnpolicy']")
	private WebElement returnPolicy;
	
	@FindBy(css="a[href='/prive']")
	private WebElement priveLink;
	
	@FindBy(css="a[href='/static_pages/privacypolicy']")
	private WebElement privacyPolicy;
	
	@FindBy(xpath="//u[text()='PRIVACY & COOKIE POLICY']")
	private WebElement privacyPolicyHeader;
	
	private String allHeader = "//b";
	
	@FindBy(css="a[href='/store_locator']")
	private WebElement storeLocator;
	
	@FindBy(xpath="//b")
	private WebElement collectiveStoresHeader;
	
	@FindAll(value=@FindBy(xpath="//h3"))
	private List<WebElement> eachStores;
	
	@FindBy(xpath="//h2[contains(text(),'Address')]")
	private WebElement addressHeader;
	
	@FindBy(xpath="//address")
	private WebElement addressText;
	
	@FindBy(css="a[href='/events']")
	private WebElement eventsLink;
	
	@FindBy(xpath="//strong")
	private WebElement eventHeader;
	
	@FindBy(css="a[href='/static_pages/careers']")
	private WebElement careersLink;
	
	@FindBy(xpath="//strong[text()='CAREERS']")
	private WebElement careersHeader;
	
	@FindBy(xpath="//h4")
	private WebElement careersText;
	
	@FindBy(css="a[href='/static_pages/about']")
	private WebElement aboutTheCollectiveLink;
	
	
	
	@FindAll(value = @FindBy(xpath="//div[@class='terms-wrapper']/p"))
	private List<WebElement> aboutTheCollectiveTextParagraphs;
	
	@FindBy(xpath="//img[@alt='The Collective']")
	private WebElement collectiveLogo;
	
	@FindBy(css="a[href='/t/categories/men/5-pkt-non-denim-jean']")
	private WebElement fivePktNonDenimJean;
	
	@FindBy(css="a[href='/t/categories/men/5-pocket-denim-jeans']")
	private WebElement fivePktDenimJeans;
	
	@FindBy(css="a[href='/t/categories/men/casual-jacket']")
	private WebElement casualJacketLink;
	
	
	@FindBy(css="a[href='/t/categories/men/casual-shirt']")
	private WebElement casualShirtLink;
	
	
	@FindBy(css="a[href='/t/categories/men/dress-shirts']")
	private WebElement dressShirtLink;
	
	
	@FindBy(css="a[href='/t/categories/men/formal-jackets']")
	private WebElement formalJacketsLink;
	
	@FindBy(css="a[href='/t/categories/men/formal-trouser']")
	private WebElement formalTrouserLink;
	
	@FindBy(css="a[href='/t/categories/men/other-knits']")
	private WebElement otherKnitsLink;
	
	@FindBy(css="a[href='/t/categories/men/polo']")
	private WebElement poloLink;
	
	@FindBy(css="a[href='/t/categories/men/rugby']")
	private WebElement rugbyLink;
	
	@FindBy(css="a[href='/t/categories/men/scarves']")
	private WebElement scarvesLink;
	
	@FindBy(css="a[href='/t/categories/men/semi-formal-jackets']")
	private WebElement semiFormalJacketLink;
	
	@FindBy(css="a[href='/t/categories/men/shorts']")
	private WebElement shortsLink;
	
	@FindBy(css="a[href='/t/categories/men/suits']")
	private WebElement suitsLink;
	
	@FindBy(css="a[href='/t/categories/men/sweat-shirt']")
	private WebElement sweatShirtLink;
	
	@FindBy(css="a[href='/t/categories/men/swim-shorts']")
	private WebElement swimShortsLink;
	
	@FindBy(css="a[href='/t/categories/men/swimwear']")
	private WebElement swimWearLink;
	
	@FindBy(css="a[href='/t/categories/men/sweater']")
	private WebElement sweaterLink;
	
	@FindBy(css="a[href='/t/categories/men/t-shirt']")
	private WebElement TShirtLink;
	
	@FindBy(css="a[href='/t/categories/men/ties']")
	private WebElement tiesLink;
	
	@FindBy(css="a[href='/t/categories/men/track-pant']")
	private WebElement trackPantLink;
	
	
	@FindBy(css="a[href='/t/categories/men/track-top']")
	private WebElement trackTopLink;
	
	@FindBy(css="a[href='/t/categories/men/waistcoat']")
	private WebElement waistcoatLink;
	
	@FindBy(xpath="//div[@class='row collective-intro']")
	private WebElement introText;
	
	@FindBy(css="a[href='/made-to-measure']")
	private WebElement madeToMeasureLink;
	
	
	@FindBy(css="a[href='/t/categories/men/sunglass']")
	private WebElement sunglassesLinkMen;

	@FindBy(css="a[href='/t/brands/men/apparel/armani-collezioni']")
	private WebElement armaniLink;
	
	@FindBy(css="a[href='/t/categories/men/jeans']")
	private WebElement jeansLink;
	
	@FindBy(css="a[href='/t/s16']")
	private WebElement newArrivalsLink;
	
	@FindBy(css="a[href='http://52.76.115.153/t/brands/men/accessories/armani-jeans']")
	private WebElement carouselImage;
	
	String carouselDelete="//td[text()='"+data.getCarouselLink()+"']/following-sibling::td[@class='actions']/a[@data-method='delete']";
	
	@FindBy(xpath="//input[@id='subscription_email']")
	private WebElement subscriptionTextbox;
	
	@FindBy(xpath="//div[@class='alert alert-notice']")
	private WebElement alertMsg;
	
	@FindBy(xpath="//input[@value='subscribe']")
	private WebElement subscribeButton;
	
	
	private String InvalidHtmlMsg="input.subscription-email:invalid";
	
	public void emailSignUpBtnClick() throws AWTException, InterruptedException{
		By css = By.cssSelector("a[href='/users/register']");
		WebElement emailSignUpBtn = driver.findElement(css);
		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , emailSignUpBtn);
		}

	public void clickBlog() {
		By css = By.cssSelector("a[href='/blogs']");
		WebElement blogLink = driver.findElement(css);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , blogLink);
		}
	
	public void hoverAccessories(){
		action.moveToElement(accessories);
	}
	
	public void hoverAccessoriesMen(){
		
		action.moveToElement(accessoriesMen);
	}
	
	public void navigateToBags(){
		By css = By.cssSelector("a[href='/t/categories/men/bags']");
		WebElement bags = driver.findElement(css);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , bags);
		
		}
	
	public void clickSales()
	{
		By css = By.cssSelector("a[href='/t/sales']");
		WebElement salesLink = driver.findElement(css);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , salesLink);
		}

	public void clickLoginLink() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		By css = By.cssSelector("a[href='/login']");
		WebElement loginLink = driver.findElement(css);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , loginLink);
	}

	public void verifySignupFunctionality() throws IOException {
		Waiting.explicitWaitVisibilityOfElement(myAccountXpath, 10);
		Assert.assertTrue(myAccountXpath.isDisplayed());

	}
	
		public void logout(){
			Actions action = new Actions(driver);
			action.moveToElement(myAccountXpath);
			By css = By.cssSelector("a[href='/logout']");
			WebElement signOutLink = driver.findElement(css);
			((JavascriptExecutor)driver).executeScript("arguments[0].click();" , signOutLink);
		}
		
		public void verifyLogout() {
         Assert.assertEquals((logoutAlert.getText().trim()),data.getLogoutMsg().trim());
		}
		
		public void hoverOverMenTab()
		{
			action.moveToElement(menTab);
		}
		
		public void navigateToAdrianoGoldschmiedFromMen(){
			By css = By.cssSelector("a[href='/t/brands/men/apparel/adriano-goldschmied']");
			WebElement adrianoGoldschmied = driver.findElement(css);
			((JavascriptExecutor)driver).executeScript("arguments[0].click();" , adrianoGoldschmied);
		}
		
		public void hoverOverWomenTab()
		{
			action.moveToElement(womenTab);
		} 
		
		public void navigateToAdrianoGoldschmiedFromWomen(){
			By css = By.cssSelector("a[href='/t/brands/women/apparel/adriano-goldschmied']");
			WebElement adrianoGoldschmied = driver.findElement(css);
			((JavascriptExecutor)driver).executeScript("arguments[0].click();" , adrianoGoldschmied);
		}
		
		public void verifyBlogStories(){
			Assert.assertTrue(blogStoriesHeader.isDisplayed(), "Blog Stories header is not displayed");
			Assert.assertTrue(blogDiv.isDisplayed(), "Blogs are displayed");
		}

		public void navigateToFAQ() {
			((JavascriptExecutor)driver).executeScript("arguments[0].click();" , FAQLink);
			
		}
		
		public void verifyFAQ() {
			
			divDisplayCheck("FAQ");

		//to check for headers
		List<WebElement> s = driver.findElements(By.xpath(allHeader));
		
		
		
		
		String fromProperty[] = data.getFAQHeader().split(":");
		
		String fromPropertyCompleteText = data.getFaqText();
		
		for(int i=0;i<s.size();i++)
		{
			Assert.assertEquals(s.get(i).getText().trim(),fromProperty[i]);			
			
		}
		
	Assert.assertEquals(div.getText().replace("\n", "").trim(),fromPropertyCompleteText);	
	
}

		public void navigateToShippingPolicy() {
			
			((JavascriptExecutor)driver).executeScript("arguments[0].click();" , shippingPolicyLink);
	
		}

		public void verifyShippingPolicy() {
			
			divDisplayCheck("shippingPolicy");

			List<WebElement> s = driver.findElements(By.xpath(allHeader));
			
			
			String fromProperty[] = data.getShippingHeader().split(":");
			
			for(int i=0;i<s.size();i++)
			{
				Assert.assertEquals(s.get(i).getText().trim(),fromProperty[i]);			
				
			}
		}

		public void navigateToReturnPolicy() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();" , returnPolicy);
			
		}

		public void verifyReturnPolicy() {
			divDisplayCheck("returnPolicy");

			List<WebElement> s = driver.findElements(By.xpath(allHeader));
			
			
			String fromProperty[] = data.getReturnsHeader().split(":");
			
			for(int i=0;i<s.size();i++)
			{
				Assert.assertEquals(s.get(i).getText().trim(),fromProperty[i]);			
				
			}
			
		}

		public void navigateToPrive() {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();" , priveLink);
		}

		public void verifyPrive() {
			System.out.println("yet to be implemented");
			
		}

		public void navigateToPrivacyPolicy() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();" , privacyPolicy);
			
		}

		public void verifyPrivacyPolicy()
		{
			privacyPolicyHeader.isDisplayed();
			divDisplayCheck("privacyPolicy");
		}
		
		public void divDisplayCheck(String section)
		{
			Assert.assertTrue(div.isDisplayed(), section+" "+"section is not displayed");
		}

		public void navigateToStoreLocator() {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();" , storeLocator);
		}

		public void verifyStoreLocator() {
			String s[] = data.getStores().split(":");
			
			for(int i=0;i<s.length;i++)
			{
				Assert.assertEquals(eachStores.get(i).getText().trim(),s[i]);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();" , eachStores.get(i));
				System.out.println("pending");
				((JavascriptExecutor) driver).executeScript("arguments[0].click();" , eachStores.get(i));
			}
		}

		public void navigateToEvents() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",eventsLink);
			
		}

		public void verifyEvents() {
		Assert.assertEquals(eventHeader.getText().trim(),data.getEventHeader(),"event header is not displayed");	
		}

		public void navigateToBrandDirectory() {
			System.out.println("blocked because of bug");
			
		}

		public void verifyBrandDirectory() {
			System.out.println("blocked because of bug");
			
		}

		public void navigateToCareers() {
			((JavascriptExecutor)driver).executeScript("arguments[0].click()",careersLink);
			
		}

		public void verifyCareers() {
		
			Assert.assertTrue(careersHeader.isDisplayed(), "careers header is not displayed");
			Assert.assertEquals(careersText.getText().replace("\n","").trim(), data.getCareersText(),"careers text is not displayed");
		}

		public void navigateToTheCollective() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",aboutTheCollectiveLink);
			
		}

		public void verifyTheCollective() {
			String verifyText[] = data.getAboutUsText().split(":");
			for(int i=0;i<aboutTheCollectiveTextParagraphs.size();i++)
			{
				Assert.assertEquals(aboutTheCollectiveTextParagraphs.get(i).getText().trim(),verifyText[i],verifyText[i]+" "+"is not displayed");
			}
			
			
			
		}

		public void verifyTheCollectiveLogo() {
			collectiveLogo.isDisplayed();
			
		}

		public void verifyTheCollectiveHomePage() {
			
			
		}

		public void navigateTo_five_Pkt_Non_Denim_Jean() {
			
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",fivePktNonDenimJean);
		}

		public void navigateTo_five_Pkt_Denim_Jean() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",fivePktDenimJeans);
			
		}

		public void navigateTo_CasualJacket() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualJacketLink);
			
		}

		public void navigateTo_DressShirts() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_FormalJackets() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_FormalTrouser() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_OtherKnits() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Polo() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Scarves() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Shorts() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_SweatShirt() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_Swimwear() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_TShirt() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_TrackPant() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_Waistcoat() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_CasualBlazer() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_CasualOuterwear() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		public void navigateTo_CasualTrouser() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Ethnic() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_FormalOuterwear() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Knitwear() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_PocketSquares() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Rugby() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_semiFormalJackets() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Suits() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_swimShorts() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_sweater() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Ties() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}
		
		public void navigateTo_Tracktop() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",casualShirtLink);
			
		}

		public void checkIntroText() {
			Assert.assertTrue(introText.isDisplayed(), "Collective introduction text is not displayed");
			Assert.assertEquals(introText.getText().replace("\n", " ").trim(), data.getIntroText());
			
		}

		public void verifyProductURLsPages() 
		{
		CollectiveBagsPageObjects bagsPage = new CollectiveBagsPageObjects(driver);
		CollectiveFivePKTDenimJeansPageObjects jeansPktPage = new CollectiveFivePKTDenimJeansPageObjects(driver);
		String everyURL[] = data.getPageURLs().split(",");
		for(int i=0;i<everyURL.length;i++)
		{
			driver.get(application.getURL()+everyURL[i]);	
			bagsPage.verifyProductsDisplay();
			Waiting.explicitWaitVisibilityOfElement(jeansPktPage.colour, 5);
			Assert.assertTrue(jeansPktPage.colour.isDisplayed(),"colour section is not displayed");
			Assert.assertTrue(jeansPktPage.size.isDisplayed(),"size section is not displayed");
			Assert.assertTrue(jeansPktPage.prices.isDisplayed(),"price section is not displayed");
			}
		}

		public void clickMadeToMeasureLink() {
		
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",madeToMeasureLink);
		}

		public void switchToOtherTab() throws InterruptedException {
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(1));
		   /* driver.close();
		    driver.switchTo().window(tabs.get(0));*/
		}

		public void changeToMobileView() {
			Dimension dimension = new Dimension(375, 627);
			driver.manage().window().setSize(dimension);
			
		}

		public void changeToTabletView() {
			Dimension dimension = new Dimension(800, 1024);
			driver.manage().window().setSize(dimension);
			
		}

		public void navigateToSunGlassesFromMen() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click()",sunglassesLinkMen);
			
		}

		public void switchToPreviousTab() {
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(0));
			
		}

		public void maximizeWindow() {
			driver.manage().window().maximize();
			
		}

		public void navigateToArmaniFromMen() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",armaniLink);
			
		}

		public void navigateToJeansFromMen() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",jeansLink);
			
		}

		public void navigateToPoloFromMen() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",poloLink);
			
		}

		public void navigateTo_newArrivals() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",newArrivalsLink);
			
		}

		public void openApplication() throws Exception  {
			setUp();
			
		}

		public void verifyCarousel() {
			Waiting.explicitWaitVisibilityOfElement(collectiveLogo, 40);
			//a[@href='http://52.76.115.153/t/brands/men/accessories/armani-jeans']
			
			//WebElement newCarouselLink= driver.findElement(By.xpath("//div[@class='slide-content main-slider-image']/a[@href='"+data.getCarouselLink()+"']"));
			WebElement newCarouselLink= driver.findElement(By.xpath("//div[@class='slide-content main-slider-image']"));
			Assert.assertTrue(newCarouselLink.isDisplayed(),"new Carousel Link is not displayed");
			
		}

		public void deleteCarousel() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.findElement(By.xpath(carouselDelete)).click();
			
		}

		public void switchToLatestWindow() {
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
		}
		public String getParentWindow(){
			String parentHandle = driver.getWindowHandle();
			return parentHandle;
		}

		public void switchToParentWindow(String s) {
			driver.switchTo().window(s);
			
		}

		public void enterSubscriptionEmail() {
			String[] split = data.getsignupEmail().split("@");
			String email = split[0]+"+"+RandomNumberGenerator.generateRandomNumber()+"@"+split[1]; 
			subscriptionTextbox.sendKeys(email);
			
		}

		public void enterExisitingSubscriptionEmail() {
			subscriptionTextbox.sendKeys(data.getsignupEmail());
			
		}

		public void verifySubscribeNewsLetterMsg() {
			Assert.assertEquals(alertMsg.getText().trim(), data.getSubscribeNewsLetterText());
			
		}

		public void verifySubscribeNewsLetterMsgExistingEmail() {
			Assert.assertEquals(alertMsg.getText().trim(), data.getSubscribeNewsLetterTextExistingEmail());
			
		}

		public void clickSubscribe() {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",subscribeButton);
			
		}
		
		public void verifyErrorMsgForNoEmailInSubscription(){
			Assert.assertTrue(driver.findElement(By.cssSelector(InvalidHtmlMsg)).isDisplayed(),"Subscrption email empty Error Msg is not displayed");
		}
		
}

