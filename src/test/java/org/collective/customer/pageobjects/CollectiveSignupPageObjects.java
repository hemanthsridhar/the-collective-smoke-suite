package org.collective.customer.pageobjects;
import org.collective.maincontroller.MainController;
import org.collective.utils.RandomNumberGenerator;
import org.collective.utils.SearchData;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.IOException;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveSignupPageObjects extends MainController{
	
   SearchData data = new SearchData();
   
	public CollectiveSignupPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='spree_user_name']")
	private WebElement signupName;
	
	@FindBy(xpath="//input[@id='spree_user_email']")
	private WebElement signupEmail;
	
	@FindBy(xpath="//input[@id='spree_user_phone_number']")
	private WebElement signUpPhoneNumber;
	
	@FindBy(xpath="//input[@id='spree_user_password']")
	private WebElement signUpPassword;
	
	@FindBy(xpath="//input[@id='spree_user_password_confirmation']")
	private WebElement signUpPasswordConfirmation;
	
	@FindBy(xpath="//input[@value='Signup']")
	private WebElement signUpBtn;
	
	@FindBy(xpath="//div[@id='errorExplanation']")
	private WebElement errorExplanation;
	
	@FindBy(xpath="//a[text()='Login as Existing Customer']")
	private WebElement loginExistingCustomer;
	
	
	public void signupUsername() throws IOException{
		Waiting.explicitWaitVisibilityOfElement(signupName, 6);
	signupName.sendKeys((data.getsignupUsername()));
	}
	
	public void signupEmail() throws IOException {
String[] split = data.getsignupEmail().split("@");
String email = split[0]+"+"+RandomNumberGenerator.generateRandomNumber()+"@"+split[1]; 
signupEmail.sendKeys(email);		
	}
	
	public void signupPhoneNumber() throws InterruptedException{
		String n = "99"+Integer.toOctalString(RandomNumberGenerator.generateEightRandomNumbers());
		signUpPhoneNumber.sendKeys(n);	
	}
	
	public void signUpPasswordAndConfirmation() throws IOException
	{
	signUpPassword.sendKeys(data.getsignupPassword());
	signUpPasswordConfirmation.sendKeys(data.getsignupPassword());
	}
	
	public void signupBtnClick() throws InterruptedException{
		signUpBtn.click();
	}

	public void clearSignUpTextboxes() {
		signupName.clear();
		signupEmail.clear();
		signUpPhoneNumber.clear();
		signUpPassword.clear();
		signUpPasswordConfirmation.clear();
	}

	public void verifyErrorMsgRegAllEmpty() {
		Assert.assertTrue(errorExplanation.isDisplayed(), "Error explanation is not displayed");
		Assert.assertEquals(errorExplanation.getText().replace("\n", " ").trim(),data.geterrorMsgForRegAllEmpty());
		
	}

	public void verifyErrorMsgRegExistingPhnoAndPwd() {
		Assert.assertTrue(errorExplanation.isDisplayed(), "Error explanation is not displayed");
		Assert.assertEquals(errorExplanation.getText().replace("\n", " ").trim(),data.geterrorMsgForRegExistingPhnoAndPwd());
		
	}

	public void verifyErrorMsgRegUniquePhnoAndPwd_NoName_No_Email() {
		Assert.assertTrue(errorExplanation.isDisplayed(), "Error explanation is not displayed");
		System.out.println("blocked due to bug");
		
	}

	public void verifyErrorMsgRegUniquePhnoPwdMismatch() {
		Assert.assertTrue(errorExplanation.isDisplayed(), "Error explanation is not displayed");
		Assert.assertEquals(errorExplanation.getText().replace("\n", " ").trim(),data.geterrorMsgForRegPwdMismatch());
	}

	public void signUpPasswordForErrorScenario() {
		
		signUpPassword.sendKeys(data.getsignupPassword());
	}
	
	public void signUpPasswordConfirmationForErrorScenario(){
		signUpPasswordConfirmation.sendKeys(data.getsignupPassword()+"4");
	}

	public void clickOnLoginAsExistingCustomer() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",loginExistingCustomer);
	}

	public void signupUsernameForWholeNumberData() {
		
		signupName.sendKeys(data.getWholeNumbers());
	}

	public void verifyErrorMsg_ErrorScenario_WholeNumbers_Name() {
	System.out.println("blocked");
	}

	public void signupUsernameForSpecialCharacterData() {
		
		signupName.sendKeys(data.getSpecialCharacters());
	}

	public void verifyErrorMsg_ErrorScenario_SpecialCharacters_Name() {
		System.out.println("blocked");
		
		
	}

	public void signupEmailExisting() {
		signupEmail.sendKeys(data.getsignupEmail());
		
	}

	public void signupPhoneNumberLessThan10() {
		signUpPhoneNumber.sendKeys(data.getSignUpPhoneNumberLessThan10());
		
	}

	public void signupPhoneNumberSpecialCharactersAndAlphabets() {
		
		signUpPhoneNumber.sendKeys(data.getsignInPassword());
	}
	
	public void verifyErrorMsg_PhoneNumber_LessThan10Characters() {
		System.out.println("blocked");
		
	}

	public void verifyErrorMsg_ExistingEmail() {
		System.out.println("blocked");
		
	}

	public void signupEmailWrongFormat() {
		signupEmail.sendKeys(data.getsignupUsername());
		
	}

	public void verifyErrorMsg_Email_invalidEmailFormat() {
		System.out.println("blocked");
		
	}
}
