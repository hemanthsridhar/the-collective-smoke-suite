package org.collective.customer.pageobjects;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import org.collective.maincontroller.MainController;
import org.collective.utils.Waiting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/*
 * @author Hemanth.Sridhar
 */
public class CollectiveSalesPageObjects extends MainController{
	
	public CollectiveSalesPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//div[@id='products_page']")
	private WebElement productsdiv;
	
	@FindAll(value={@FindBy(xpath="//div[@id='products_page']/ul/li")})
	private WebElement productsList;
	
	@FindBy(xpath="//input[@id='ARMANI_JEANS']")
	private WebElement armaniCheckbox;
	
	@FindAll(value={@FindBy(xpath="//a[text()='Armani Jeans Laptop Bag']")})
	private WebElement armaniDisplayCheck;
	
	@FindBy(xpath="//ul[@class='image-list no-pad clearfix']/li[1]")
	private WebElement firstProduct;
	
	@FindBy(css="a[href='/t/categories/men/bags?page=2']")
	private WebElement paginatorNextClick;
	
	@FindAll(value = @FindBy (xpath="//li[@class='prod-price-discount']/a"))
	private List<WebElement> actualPrice;
	
	@FindAll(value = @FindBy(xpath="//li[@class='prod-price prod_discount']/a"))
	private List<WebElement> discountedPrice;
	
	
	public void verifyPagination()
	{
		Assert.assertTrue(firstProduct.isDisplayed());
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , paginatorNextClick);		
		Waiting.explicitWaitVisibilityOfElement(firstProduct, 15);
		Assert.assertTrue(firstProduct.isDisplayed());
	    
	}
	
	public void assertDiscountAndActualPrice() throws ParseException{
	Assert.assertTrue(checkDiscountAndActualPrice(), "Discount Price is greater than the Actual Price in Sales page");
	}
	
	public boolean checkDiscountAndActualPrice() throws ParseException{
		for(int i=0;i<actualPrice.size();i++)
		{
		Number APrice = NumberFormat.getNumberInstance(Locale.getDefault()).parse(actualPrice.get(i).getText());
		Number DPrice = NumberFormat.getNumberInstance(Locale.getDefault()).parse(discountedPrice.get(i).getText());
		if(APrice.intValue() > DPrice.intValue())
		{
		 return true;
		}
		}
		return false;
	}
	}
	
