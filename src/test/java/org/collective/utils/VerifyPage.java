package org.collective.utils;

import java.util.List;

import org.collective.maincontroller.MainController;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class VerifyPage extends MainController{
	
	public VerifyPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@id='products_page']")
	private WebElement productsdiv;
	

	@FindAll(value={@FindBy(xpath="//div[@id='products_page']/ul/li")})
	private WebElement productsList;
	
	@FindBy(xpath="//div[@id='brands']")
	private WebElement brands;
	
	@FindBy(xpath="//div[@id='size_color']/div[1]")
	private WebElement size;
	
	@FindBy(xpath="//div[@id='size_color']/div[2]")
	private WebElement colour;
	
	@FindAll(value = @FindBy(xpath="//li/a[2]"))
	private List<WebElement> categoryType;
	
	public void verifyFivePktDenimJeansPage(String verifyText) {
		Assert.assertTrue(productsdiv.isDisplayed(),"products are not displayed");
			
			Assert.assertTrue(productsList.isDisplayed(),"products are not displayed");
			
			Assert.assertTrue(brands.isDisplayed(), "brands section is not displayed");
			
			Assert.assertTrue(size.isDisplayed(), "size section is not displayed");
			
			Assert.assertTrue(colour.isDisplayed(), "colour section is not displayed");

			for(int i=0;i<categoryType.size();i++)
			{
			Assert.assertEquals(categoryType.get(i).getText().trim(),verifyText);
		}
		}

}
