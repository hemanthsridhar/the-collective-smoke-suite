package org.collective.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TodaysDetails {

	public static String currentDate(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String s = dateFormat.format(date);
		String[] a = s.split("/");
		return a[0];
	}
	
	public static String currentMonthNumber(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String s = dateFormat.format(date);
		String a[] = s.split("/");
		return a[1];
	}
	
	public static String currentMonthYearNumber(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String s = dateFormat.format(date);
		String a[] = s.split("/");
		return a[2];
	}
}
